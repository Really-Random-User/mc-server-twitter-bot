#!/bin/bash
#This script requires the following packages to work
#screen, rainbowstream (can be installed with pip3), nmap, bash
#
##Must have rainbowstream already SET UP see https://www.tecmint.com/rainbow-stream-command-line-twitter-client-linux/
#
##CONFIGURATION, defaults only work for one local MC server running on port 25565
#This is the address to connect to server ex. hypixel.net
address=localhost
#port number for the minecraft server
port=25565
#name of screen session to create (for running rainbowstream) use that if you are planning to set up more than one server monitor on one device
#DO NOT USE SPACES
screenname=rbowstream
#Text before number of players and time statement
textbefore="There are"
#Script uses nmap to detect if players are/ online for a minecraft server
#start up rainbowstream
##DO NOT EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU ARE DOING
sleep 20
isrstreamup=$( screen -list | grep -o $screenname )
if [ "$isrstreamup" = $screenname ]
then
	echo twitter already running!!
else
	screen -dmS $screenname
	sleep 2
	screen -S $screenname -X stuff 'rainbowstream\n'
	sleep 2
	date=$( date )
	run=yes
	compareVar=notsetyet
fi
while [ "$run" = "yes" ]
do
	anyoneOn=$( nmap -A -p $port $address | grep -o "\<[0-9]*\/[0-9]*" | grep -v "$port" )
	sleep 10
	if [ "$anyoneOn" = "$compareVar" ]
	then
		echo no change in players and therefore not tweeting press ctrl+c to quit
		sleep 20
	else
		compareVar=$anyoneOn
		sleep 2
		date=$( date "+%F-%T" )
		screen -S $screenname -X stuff "t $textbefore $compareVar players online at $date \n"
		sleep 3
		screen -S $screenname -X stuff " \n"
		sleep 3
		screen -S $screenname -X stuff " \n"
		fi
done
