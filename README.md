# MC server Twitter Bot

This is a Twitter Bot Written in bash that will Tweet whenever there is a change in the number of players online

# Requirements
Bash (Obviously), GNU screen, nmap, rainbowstream see install instructions for rainbowstream [here](https://www.tecmint.com/rainbow-stream-command-line-twitter-client-linux/) The rest can probably be installed on Debian based Distributions with apt (sudo apt install screen && sudo apt install nmap) and most distributions come with bash preinstalled. 

# How to use
Download the twitbot.sh file in this repo, near the beggining of the file you will see configuration statements, such as port=25565, change these as you see fit, then run
the file by using the bash command. You MUST have rainbowstream set up and attached to a Twitter account, you must also have nmap installed as well as GNU screen.

# Just want to post to Discord see this project [Link](https://gitlab.com/Really-Random-User/discord-posting-bot)